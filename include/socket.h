#ifndef SOCKET_CONN_H
#define SOCKET_CONN_H

#define BACKLOG 5

void sigchld_handler(int signal);

void set_nonblock(int fd);

int wait_for_connection(int fd);

void *get_in_addr(struct sockaddr *sa);

int passive_socket(char *service, char *protocol, char *localip);

int connect_socket(char *host, char *service);

int wait_for_connection(int fd);
#endif

