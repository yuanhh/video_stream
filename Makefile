CC = gcc
CFLAGS = -std=gnu99 -g -Wall -I$(INCLUDE_DIR)
INCLUDE_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./obj
LDFLAGS = -lavutil -lavformat -lavcodec -lswscale -lz -lm

EXTRACT_PPM = extract_ppm
STREAM = stream
STREAM_OBJ = $(patsubst %, $(OBJ_DIR)/%, $(_STREAM_OBJ))
_STREAM_OBJ = udp_stream.o socket.o

all: $(EXTRACT_PPM) $(STREAM)

$(EXTRACT_PPM): $(SRC_DIR)/$(EXTRACT_PPM).c
	$(CC) -o $@ $< $(CFLAGS) $(LDFLAGS)

$(STREAM): $(STREAM_OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	rm -f $(STREAM) $(EXTRACT_PPM)
	rm -rf $(OBJ_DIR)/*
