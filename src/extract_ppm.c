// tutorial01.c
// Code based on a tutorial by Martin Bohme (boehme@inb.uni-luebeckREMOVETHIS.de)
// Tested on Gentoo, CVS version 5/01/07 compiled with GCC 4.1.1
// With updates from https://github.com/chelyaev/ffmpeg-tutorial
// Updates tested on:
// LAVC 54.59.100, LAVF 54.29.104, LSWS 2.1.101 
// on GCC 4.7.2 in Debian February 2015

// A small sample program that shows how to use libavformat and libavcodec to
// read video from a file.
//
// Use
//
// gcc -o tutorial01 tutorial01.c -lavformat -lavcodec -lswscale -lz
//
// to build (assuming libavformat and libavcodec are correctly installed
// your system).
//
// Run using
//
// tutorial01 myvideofile.mpg
//
// to write the first five frames from "myvideofile.mpg" to disk in PPM
// format.

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>

// compatibility with newer API
#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(55,28,1)
#define av_frame_alloc avcodec_alloc_frame
#define av_frame_free avcodec_free_frame
#endif

AVFrame *new_RGBframe(int width, int height)
{
    AVFrame *frame = av_frame_alloc();
    uint8_t *buffer;
    int numBytes;

    // Determine required buffer size and allocate buffer
    numBytes = avpicture_get_size(PIX_FMT_RGB24, width, height);
    buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));

    // Assign appropriate parts of buffer to image planes in pFrameRGB
    // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
    // of AVPicture
    avpicture_fill((AVPicture *)frame, buffer, PIX_FMT_RGB24, width, height);

    return frame;
}

void SaveFrame(AVFrame *pFrame, int width, int height, int frame_index)
{
    DIR *dir = opendir("tmp");
    FILE *pFile;
    char szFilename[32] = {0};
    int  y;

    // open folder
    if (dir == NULL && errno == ENOENT)
    {
        if (mkdir("tmp", S_IRUSR | S_IWUSR | S_IXUSR) == -1) return;
    }
    
    // Open file
    sprintf(szFilename, "./tmp/frame%d.ppm", frame_index);
    pFile = fopen(szFilename, "wb");
    if (pFile == NULL)
        return;

    // Write header
    fprintf(pFile, "P6\n%d %d\n255\n", width, height);

    // Write pixel data
    for(y = 0; y < height; y++)
    {
        fwrite(pFrame->data[0] + y * pFrame->linesize[0], 1, width * 3, pFile);
    }

    // Close dir
    closedir(dir);
    // Close file
    fclose(pFile);
}

AVCodecContext *stream_component_open(AVFormatContext *pFormatCtx,
        int stream_index)
{
    AVCodecContext  *pCodecCtxOrig = NULL, *pCodecCtx = NULL;
    AVCodec         *pCodec = NULL;

    if (stream_index < 0 ||
            (unsigned int)stream_index >= pFormatCtx->nb_streams)
    {
        return 0;
    }
    // Get a pointer to the codec context for the video stream
    pCodecCtxOrig = pFormatCtx->streams[stream_index]->codec;

    // Find the decoder for the video stream
    pCodec = avcodec_find_decoder(pCodecCtxOrig->codec_id);
    if(!pCodec)
    {
        fprintf(stderr, "Unsupported codec!\n");
        return 0;
    }

    // copy context
    pCodecCtx = avcodec_alloc_context3(pCodec);
    if(avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0)
    {
        fprintf(stderr, "Couldn't copy codec context");
        return 0;
    }

    // Open codec
    if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
    {
        fprintf(stderr, "Could not open codec!\n");
        return 0;
    }

    avcodec_close(pCodecCtxOrig);
    return pCodecCtx;
}

int find_stream_index(const AVFormatContext *pFormatCtx)
{
    unsigned int index = 0;

    for(index = 0; index < pFormatCtx->nb_streams; ++index)
    {
        if(pFormatCtx->streams[index]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            return index;
        }
    }
    return -1;
}

int retrieve_stream_info(AVFormatContext *pFormatCtx, char *name)
{
    // Retrieve stream information
    if(avformat_find_stream_info(pFormatCtx, NULL)<0)
    {
        fprintf(stderr, "Counldn't find stream information!\n");
        return -1;
    }

    // Dump information about file onto standard error
    av_dump_format(pFormatCtx, 0, name, 0);

    return 0;
}

int main(int argc, char *argv[])
{
    // Initalizing these to NULL prevents segfaults!
    AVFormatContext *pFormatCtx = NULL;
    int video_stream_index = -1;
    int frame_index;
    AVCodecContext *pCodecCtx = NULL;
    AVFrame *pFrame = NULL;
    AVFrame *pFrameRGB = NULL;
    AVPacket packet;
    int frameFinished;
    struct SwsContext *sws_ctx = NULL;

    // Register all formats and codecs
    av_register_all();

    switch (argc)
    {
        case 2:
            // Open video file
            if(avformat_open_input(&pFormatCtx, argv[1], NULL, NULL)!=0)
            {
                fprintf(stderr, "Couldn't open file!\n");
                return -1;
            }
            break;
        default:
            fprintf(stderr, "Usage: %s <movie name>\n", argv[0]);
            return -1;
            break;
    }

    retrieve_stream_info(pFormatCtx, argv[1]);

    // Find the first video stream
    video_stream_index = find_stream_index(pFormatCtx);
    if (video_stream_index < 0)
    {
        fprintf(stderr, "Didn't find a video stream!\n");
        return -1;
    }

    pCodecCtx = stream_component_open(pFormatCtx, video_stream_index);
    if (pCodecCtx == NULL)
    {
        return -1;
    }

    // Allocate video frame
    pFrame=av_frame_alloc();

    // Allocate an AVFrame structure
    pFrameRGB = new_RGBframe(pCodecCtx->width, pCodecCtx->height);
    if(pFrameRGB == NULL || pFrame == NULL)
    {
        return -1;
    }

    sws_ctx = sws_getContext
        (
         pCodecCtx->width,
         pCodecCtx->height,
         pCodecCtx->pix_fmt,
         pCodecCtx->width,
         pCodecCtx->height,
         PIX_FMT_RGB24,
         SWS_BILINEAR,
         NULL,
         NULL,
         NULL
        );

    frame_index = 0;
    while (av_read_frame(pFormatCtx, &packet) >= 0)
    {
        // Is this a packet from the video stream?
        if (packet.stream_index==video_stream_index)
        {
            // Decode video frame
            avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

            // Did we get a video frame?
            if (frameFinished)
            {
                // Convert the image from its native format to RGB
                sws_scale
                    (
                     sws_ctx,
                     (uint8_t const * const *)pFrame->data,
                     pFrame->linesize, 0, pCodecCtx->height,
                     pFrameRGB->data, pFrameRGB->linesize
                    );

                // Save the frame to disk
                SaveFrame(pFrameRGB, pCodecCtx->width, pCodecCtx->height, ++frame_index);
            }
        }
        // Free the packet that was allocated by av_read_frame
        av_free_packet(&packet);
    }
    // Free the RGB image
    av_frame_free(&pFrameRGB);

    // Free the YUV frame
    av_frame_free(&pFrame);

    // Close the codecs
    avcodec_close(pCodecCtx);

    // Close the video file
    avformat_close_input(&pFormatCtx);

    return 0;
}
