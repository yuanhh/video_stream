#define _POSIX_SOURCE
#include <dirent.h>
#include <errno.h>
#undef _POSIX_SOURCE
#include <stdio.h>
#include <stdint.h>     /* uint8_t */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>     /* offsetof() */
#include <sys/select.h> /* select() */
#include <sys/wait.h>
#include <sys/types.h>  /* pid */
#include <sys/socket.h> /* sendto() */
#include <netinet/in.h> /* struct sockaddr_in */

#include "socket.h"

#define BUFFERSIZE 1024
#define PORT "8000"
#define DIR_PATH "./tmp/"

int send_file(const char *file_name, int fd, struct sockaddr *cli, socklen_t len) {

    uint8_t *buffer = 0;
    FILE *fp = fopen(file_name, "rb");
    int nbytes = 0;

    if (fp == NULL)
    {
        perror("ERROR on open file\r\n");
        return 0;
    }

    buffer = (uint8_t *)calloc(BUFFERSIZE, sizeof(uint8_t));

    for (;;)
    {
        memset(buffer, 0, BUFFERSIZE);
        nbytes = fread(buffer, sizeof(uint8_t), BUFFERSIZE, fp);
        if (nbytes)
        {
            sendto(fd, buffer, nbytes, 0, cli, len);
        }
        else
        {
            if (ferror(fp))
                perror("Error reading");
            else if (feof(fp))
                perror("EOF found");

            break;
        }
    }

    fclose(fp);
    return 0;
}

int list_files(int fd, struct sockaddr *cli, socklen_t len)
{
    DIR *dirp;
    struct dirent *entryp, *result;
    int nameMax;

    /* On Linux, NAME_MAX is defined in <limits.h>. However, this limit
     * may vary across file systems, so we really should use pathconf()
     * to find the true limit for this file system. */
    nameMax = pathconf(DIR_PATH, _PC_NAME_MAX);
    if (nameMax == -1)          /* Indeterminate or error */
        nameMax = 255;          /* So take a guess */

    entryp = malloc(offsetof(struct dirent, d_name) + nameMax + 1);
    if (entryp == NULL)
    {
        perror("malloc");
        return -1;
    }
    /* Open the directory - on failure print an error and return */
    dirp = opendir(DIR_PATH);
    if (dirp == NULL)
    {
        perror("opendir() error");
        return -1;
    }
    /* Look at each of the entries in this directory */
    for (;;)
    {
        errno = readdir_r(dirp, entryp, &result);
        switch (errno)
        {
            case 0:
                break;
            case EACCES:
                fprintf(stderr, "Permission denied.\n");
                return -1;
                break;
            case ENOENT:
                fprintf(stderr, "Directory does not exist.\n");
                return -1;
                break;
            default:
                perror("readdir_r");
                return -1;
                break;

        }
        if (result == NULL)     /* End of stream */
            break;

        /* Skip . and .. */
        if (entryp->d_name[0] == '.')
        {
            continue;
        }

        send_file(entryp->d_name, fd, cli, len);
        printf("%s\n", entryp->d_name);
    }
    closedir(dirp);
    return 0;
}

int main(int argc, char *argv[])
{
    int listen_fd = 0;
    char buffer[BUFFERSIZE] = {0};

    switch (argc)
    {
        case 1:
            listen_fd = passive_socket(PORT, "udp", 0);
            break;
        case 2:
            listen_fd = passive_socket(argv[1], "udp", 0);
            break;
        default:
            break;
    }

    for (;;)
    {
        memset(buffer, 0, BUFFERSIZE);

        int status;
        pid_t pid;

        struct sockaddr_in cli;
        socklen_t len;

        recvfrom(listen_fd, buffer, BUFFERSIZE, 0, (struct sockaddr *)&cli, &len);

        pid = fork();
        if (pid == 0)
        {
            /* This is the child process. */
            status = list_files(listen_fd, (struct sockaddr *)&cli, len);
            if (status < 0)
            {
                exit(EXIT_FAILURE);
            }
            else
                exit(EXIT_SUCCESS);
        }
        else if (pid < 0)
        {
            /* The fork failed.  Report failure.  */
            status = -1;
        }
        else
        {
            /* This is the parent process. */
        }
    }
    return 0;
}
